import { expect } from "chai";
import { Library } from "../../src/library/index";

describe("Library", () => {
  const lib = new Library();
  it("test stringToNumber", () => {
    expect(lib.stringToNumber("a")).equal(0);
    expect(lib.stringToNumber("ab")).equal(1);
    expect(lib.stringToNumber("ba")).equal(29);
  });

  it("test int2base", () => {
    expect(lib.int2base(4n, 36n)).equal("4");
    expect(lib.int2base(10n, 36n)).equal("A");
  });

  it("test toText", () => {
    expect(lib.toText(0)).equal("a");
    expect(lib.toText(1)).equal("b");
    expect(lib.toText(29)).equal("ba");
  });

  it("test parseAddress", () => {
    expect(() => {
      lib.parseAddress("some:123:21");
    }).to.throw("Address is invalid");

    expect(lib.parseAddress("some:4:21:1:20")).to.deep.equals({
      hexAddr: "some",
      wall: "4",
      shelf: "21",
      volume: "01",
      page: "020"
    });
  });

  it("test getPage", () => {
    expect(lib.getPage("as:4:3:30:2").length).equal(3200);
  });

  it("test getTitle", () => {
    expect(lib.getTitle("as:4:3:30:2").length).equal(25);
  });
});
