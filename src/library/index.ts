const LENGTH_OF_PAGE = 3200;
const LENGTH_OF_TITLE = 25;
const LOC_MULT = BigInt(30n ** 3200n);
const TITLE_MULT = BigInt(Math.pow(30, 25));

//29 output letters: alphabet plus comma, space, and period
//alphanumeric in hex address (base 36): 3260
//in wall: 4
//in shelf: 5
//in volumes: 32
const PAGES = 410;
//letters per page: 3239
//titles have 25 char

function parseBigInt(value: string, radix: number): bigint {
  const size = 10;
  let sign = 1n;
  if (value[0] == "-") {
    sign = -1n;
    value = value.slice(1);
  }
  let i = value.length % size || size;

  const factor = BigInt(radix ** size),
    parts = [value.slice(0, i)];

  while (i < value.length) parts.push(value.slice(i, (i += size)));
  return (
    parts.reduce((r, v) => r * factor + BigInt(parseInt(v, radix)), 0n) * sign
  );
}
export class Library {
  getTitle(address: string) {
    const { hexAddr, wall, shelf, volume, page } = this.parseAddress(address);
    const locInt = parseInt(volume + shelf + wall);
    let key = parseBigInt(hexAddr, 36);
    key -= BigInt(locInt) * TITLE_MULT;
    const str36 = this.int2base(key, 36n);
    const result = this.toText(parseBigInt(str36, 36));
    if (result.length > LENGTH_OF_TITLE) {
      return result.slice(0, LENGTH_OF_TITLE);
    }
    return result;
  }

  getPage(address: string): string {
    const { hexAddr, wall, shelf, volume, page } = this.parseAddress(address);

    const locInt = parseInt(page + volume + shelf + wall);
    let key = parseBigInt(hexAddr, 36);
    key -= BigInt(locInt) * LOC_MULT;
    const str36 = this.int2base(key, 36n);
    const result = this.toText(parseBigInt(str36, 36));

    if (result.length > LENGTH_OF_PAGE) {
      return result.slice(0, LENGTH_OF_PAGE);
    }
    return result;
  }

  parseAddress(address: string) {
    const parts = address.split(":");
    if (parts.length != 5) {
      throw new Error("Address is invalid");
    }
    // eslint-disable-next-line prefer-const
    let [hexAddr, wall, shelf, volume, page] = parts;
    volume = volume.padStart(2, "0");
    page = page.padStart(3, "0");

    return {
      hexAddr,
      wall,
      shelf,
      volume,
      page
    };
  }

  toText(x: number | bigint) {
    x = BigInt(x);
    // оптимизация
    // минус наверное не нужен
    const res = this.int2base(x, 29n, "abcdefghijklmnopqrstuvwxyz, .");
    if (res[0] == "-") {
      return res.slice(1);
    }
    return res;
  }

  stringToNumber(iString: string) {
    const digs = "abcdefghijklmnopqrstuvwxyz, .";

    let result = 0;
    for (let x = 0; x < iString.length; x++) {
      result += digs.indexOf(iString[iString.length - x - 1]) * Math.pow(29, x);
    }
    return result;
  }

  int2base(
    x: bigint,
    base: bigint,
    digs = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
  ) {
    let sign = 0n;
    if (x < 0n) sign = -1n;
    else if (x == 0n) return digs[0];
    else sign = 1n;

    x = x * sign;
    const digits = [];

    while (x) {
      digits.push(digs[Number(x % base)]);
      x = x / base;
    }

    if (sign < 0) {
      digits.push("-");
    }

    digits.reverse();
    return digits.join("");
  }
}
